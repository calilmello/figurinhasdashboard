package br.com.figurinhas.dashboard.repositoryInterface;

import java.util.List;

import br.com.figurinhas.dashboard.model.Editora;

public interface EditoraInterfaceRepository{
	
	public Editora cadastrarEditora(String nome);

	public List<Editora> buscarEditoras();

	public Editora atualizar(Object obj);

	public Object pesquisar(Object obj);

	public void excluir(Object id);


}
