package br.com.figurinhas.dashboard.repositoryInterface;

import java.util.List;

import br.com.figurinhas.dashboard.model.Album;

public interface AlbumInterfaceRepository{
	

	public List<Album> buscarAlbum();

	public Album atualizar(Object obj);

	public Object pesquisar(Object obj);

	public void excluir(Object id);

	public Album cadastrarAlbum(Object obj);


}
