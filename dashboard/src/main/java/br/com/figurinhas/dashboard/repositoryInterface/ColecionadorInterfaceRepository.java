package br.com.figurinhas.dashboard.repositoryInterface;

import java.util.List;

import br.com.figurinhas.dashboard.model.Album;
import br.com.figurinhas.dashboard.model.Colecionador;

public interface ColecionadorInterfaceRepository{
	
	public Colecionador cadastrarColecionador(Object obj);

	public List<Colecionador> buscarColecionadores();

	public Colecionador atualizar(Object obj);

	public Object pesquisar(Object obj);

	public void excluir(Object id);



}
