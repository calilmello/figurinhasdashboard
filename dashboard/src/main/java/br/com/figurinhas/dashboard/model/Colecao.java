package br.com.figurinhas.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Colecao  {
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@JsonProperty("id")
	private long id;
	
	@JsonProperty("colecionador")
	private Colecionador colecionador;
	
	@JsonProperty("album")
	private Album album;
	
	@JsonProperty("figurinhas")
	private String figurinha;
	
	
	public Colecao() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Colecionador getColecionador() {
		return colecionador;
	}

	public void setColecionador(Colecionador colecionador) {
		this.colecionador = colecionador;
	}

	public Album getAlbum() {
		return album;
	}

	public void setAlbum(Album album) {
		this.album = album;
	}

	public String getFigurinha() {
		return figurinha;
	}

	public void setFigurinha(String figurinha) {
		this.figurinha = figurinha;
	}

}
