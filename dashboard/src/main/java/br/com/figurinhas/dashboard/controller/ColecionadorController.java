package br.com.figurinhas.dashboard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.figurinhas.dashboard.model.Colecionador;
import br.com.figurinhas.dashboard.service.ColecionadorService;

@Controller
public class ColecionadorController {
	

	@Autowired
	ColecionadorService servico;
	
	@RequestMapping("/cadastrarColecionador")
	public String colecionadorHome() {
		return "colecionador/cadastrarColecionador";
	}
	
	

	@RequestMapping(value="salvarColecionador", method=RequestMethod.POST)
	public String salvarColecionador(HttpServletRequest request, Model model) {
		
		Colecionador colecionador = new Colecionador();
		
		colecionador.setNome(request.getParameter("nome"));
		colecionador.setEmail(request.getParameter("email"));
		
		colecionador = (Colecionador) servico.cadastrar(colecionador);
		
		return colecionadorHome();
	}
	
	@RequestMapping(value = "buscarColecionadores", method = RequestMethod.GET)
	public String buscarColecionadores(Model model) {
		
		List<Colecionador> lista = (List<Colecionador>) servico.buscar();
		model.addAttribute("listaColecionador", lista);
		
		return "colecionador/buscarColecionador";
	}
	
	
	@RequestMapping(value="atualizarColecionadorHome/{id}", method=RequestMethod.GET)
	public String atualizarColecionadorHome(@PathVariable("id") Long id, Model model) {
		
		Colecionador colecionador = (Colecionador) servico.pesquisar(id);
				
		model.addAttribute("colecionador", colecionador);
				
		return "colecionador/atualizarColecionador";
	}
	
	@RequestMapping(value="excluirColecionador/{id}", method=RequestMethod.GET)
	public String excluirColecionador(@PathVariable("id") Long id, Model model) {
		
		servico.excluir(id);
		
		return buscarColecionadores(model);
	}
	
	@RequestMapping(value="atualizarColecionador", method=RequestMethod.POST)
	public String atualizarColecionador(HttpServletRequest request, Model model) {
		
		long id = Long.parseLong(request.getParameter("id"));
		String nome = request.getParameter("nome");
		String email = request.getParameter("email");
		
		Colecionador colecionador = new Colecionador();
		colecionador.setId((Long)id);
		colecionador.setEmail(email);
		colecionador.setNome(nome);
		
		servico.atualizar(colecionador);
		
		return buscarColecionadores(model);
	}
	
}
