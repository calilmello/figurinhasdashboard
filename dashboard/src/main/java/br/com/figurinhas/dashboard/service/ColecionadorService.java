package br.com.figurinhas.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.dashboard.repository.ColecionadorRepository;
import br.com.figurinhas.dashboard.serviceInterface.CrudInterface;

@Service
public class ColecionadorService implements CrudInterface{

	@Autowired
	ColecionadorRepository repositorio;


	@Override
	public Object cadastrar(Object obj) {
		return repositorio.cadastrarColecionador(obj);
	}

	@Override
	public Object buscar() {
		return repositorio.buscarColecionadores();
	}

	@Override
	public Object atualizar(Object obj) {
		return repositorio.atualizar(obj);
	}

	@Override
	public Object pesquisar(Object obj) {
		return repositorio.pesquisar(obj);
	}

	@Override
	public void excluir(Object id) {
		repositorio.excluir(id);
	}
	
}
