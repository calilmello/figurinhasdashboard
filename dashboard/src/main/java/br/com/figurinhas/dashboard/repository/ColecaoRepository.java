package br.com.figurinhas.dashboard.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.figurinhas.dashboard.model.Colecao;
import br.com.figurinhas.dashboard.model.Editora;
import br.com.figurinhas.dashboard.repositoryInterface.ColecaoInterfaceRepository;

@Repository
public class ColecaoRepository implements ColecaoInterfaceRepository{

	@Value("${server.api}")
	private String serverApi;
	
	@Override
	public Colecao cadastrarColecao(Object obj) {
		
		Colecao colecao = (Colecao) obj;
		
		RestTemplate rest = new RestTemplate();
		HttpEntity<Colecao> request = new HttpEntity<>(colecao);
		Colecao colecaoResponse = rest.postForObject(serverApi+"/figurinhas/colecao", request, Colecao.class);
		
		return colecaoResponse;
	}

	@Override
	public Colecao atualizar(Object obj) {
		
		Colecao colecao = (Colecao) obj;
		RestTemplate rest = new RestTemplate();
		HttpEntity<Colecao> request = new HttpEntity<>(colecao);
		rest.put(serverApi+"/figurinhas/colecao", request);
		
		return colecao;
	}

	@Override
	public List<Colecao> buscarColecao() {
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Colecao[]> response = rest.getForEntity(serverApi+"/figurinhas/colecao", Colecao[].class);
		List<Colecao> listaColecao = Arrays.asList(response.getBody());
		return listaColecao;
	}

	@Override
	public Object pesquisar(Object obj) {
		
		long id = (long) obj;
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Colecao> response = rest.getForEntity(serverApi+"/figurinhas/colecao/"+id, Colecao.class);
		Colecao colecao = response.getBody();
		
		return colecao;
	}

	@Override
	public void excluir(Object obj) {
		long id = (long) obj;
		RestTemplate rest = new RestTemplate();
		rest.delete(serverApi+"/figurinhas/colecao/"+id);
		
	}

	
}
