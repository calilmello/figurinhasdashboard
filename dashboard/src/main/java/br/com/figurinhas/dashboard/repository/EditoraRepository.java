package br.com.figurinhas.dashboard.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.figurinhas.dashboard.model.Editora;
import br.com.figurinhas.dashboard.repositoryInterface.EditoraInterfaceRepository;

@Repository
public class EditoraRepository implements EditoraInterfaceRepository{

	@Value("${server.api}")
	private String serverApi;
	
	@Override
	public Editora cadastrarEditora(String nome) {
		
		Editora editora = new Editora(nome);
		
		RestTemplate rest = new RestTemplate();
		HttpEntity<Editora> request = new HttpEntity<>(editora);
		Editora editoraResponse = rest.postForObject(serverApi+"/figurinhas/editora", request, Editora.class);
		
		return editoraResponse;
	}

	@Override
	public Editora atualizar(Object obj) {
		
		Editora editora = (Editora) obj;
		RestTemplate rest = new RestTemplate();
		HttpEntity<Editora> request = new HttpEntity<>(editora);
		rest.put(serverApi+"/figurinhas/editora", request);
		
		return editora;
	}

	@Override
	public List<Editora> buscarEditoras() {
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Editora[]> response = rest.getForEntity(serverApi+"/figurinhas/editora", Editora[].class);
		List<Editora> listaEditoras = Arrays.asList(response.getBody());
		return listaEditoras;
	}

	@Override
	public Object pesquisar(Object obj) {
		
		long id = (long) obj;
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Editora> response = rest.getForEntity(serverApi+"/figurinhas/editora/"+id, Editora.class);
		Editora editora = response.getBody();
		
		return editora;
	}

	@Override
	public void excluir(Object obj) {
		long id = (long) obj;
		RestTemplate rest = new RestTemplate();
		rest.delete(serverApi+"/figurinhas/editora/"+id);
		
	}
	
}
