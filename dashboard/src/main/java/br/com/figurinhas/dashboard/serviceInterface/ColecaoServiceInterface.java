package br.com.figurinhas.dashboard.serviceInterface;

import br.com.figurinhas.dashboard.model.Colecao;

public interface ColecaoServiceInterface {

	public Colecao cadastrarColecao(String nome);
	public Colecao atualizarColecao(Colecao id);
	
}
