package br.com.figurinhas.dashboard.serviceInterface;

import br.com.figurinhas.dashboard.model.Colecionador;

public interface ColecionadorServiceInterface {

	public Colecionador cadastrar(String nome);
	public Colecionador atualizar(Colecionador id);
	
}
