package br.com.figurinhas.dashboard.repositoryInterface;

import java.util.List;

import br.com.figurinhas.dashboard.model.Colecao;

public interface ColecaoInterfaceRepository{
	
	public Colecao cadastrarColecao(Object obj);

	public List<Colecao> buscarColecao();

	public Colecao atualizar(Object obj);

	public Object pesquisar(Object obj);

	public void excluir(Object id);


}
