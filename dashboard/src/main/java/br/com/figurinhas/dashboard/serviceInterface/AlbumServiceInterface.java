package br.com.figurinhas.dashboard.serviceInterface;

import br.com.figurinhas.dashboard.model.Album;

public interface AlbumServiceInterface {

	public Album cadastrarAlbum(String nome);
	public Album atualizarAlbum(Album id);
	
}
