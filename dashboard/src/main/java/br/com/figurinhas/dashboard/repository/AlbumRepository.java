package br.com.figurinhas.dashboard.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.figurinhas.dashboard.model.Album;
import br.com.figurinhas.dashboard.repositoryInterface.AlbumInterfaceRepository;

@Repository
public class AlbumRepository implements AlbumInterfaceRepository{

	@Value("${server.api}")
	private String serverApi;
	
	@Override
	public Album cadastrarAlbum(Object obj) {
		
		Album album = (Album) obj;
		
		RestTemplate rest = new RestTemplate();
		HttpEntity<Album> request = new HttpEntity<>(album);
		Album albumResponse = rest.postForObject(serverApi+"/figurinhas/album", request, Album.class);
		
		return albumResponse;
	}

	@Override
	public List<Album> buscarAlbum() {
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Album[]> response = rest.getForEntity(serverApi+"/figurinhas/album", Album[].class);
		List<Album> listaAlbuns = Arrays.asList(response.getBody());
		return listaAlbuns;
	}

	@Override
	public Album atualizar(Object obj) {
		
		Album album = (Album) obj;
		RestTemplate rest = new RestTemplate();
		HttpEntity<Album> request = new HttpEntity<>(album);
		rest.put(serverApi+"/figurinhas/album", request);
		
		return album;
	}

	@Override
	public Object pesquisar(Object obj) {
		
		long id = (long) obj;
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Album> response = rest.getForEntity(serverApi+"/figurinhas/album/"+id, Album.class);
		Album album = response.getBody();
		
		return album;
	}

	@Override
	public void excluir(Object obj) {
		long id = (long) obj;
		RestTemplate rest = new RestTemplate();
		rest.delete(serverApi+"/figurinhas/album/"+id);
		
	}
}
