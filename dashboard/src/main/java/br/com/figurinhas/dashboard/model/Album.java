package br.com.figurinhas.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Album {

	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nome")
	private String nome;
	
	@JsonProperty("ano")
	private int ano;
	
	@JsonProperty("editora")
	private Editora Editora;
	
	@JsonProperty("quantidadeFigurinhas")
	private int QtdFigurinhas;
	
	@JsonProperty("imagem")
	private String ImgAlbum;
	
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public Editora getEditora() {
		return Editora;
	}

	public void setEditora(Editora editora) {
		Editora = editora;
	}

	public int getQtdFigurinhas() {
		return QtdFigurinhas;
	}

	public void setQtdFigurinhas(int qtdFigurinhas) {
		QtdFigurinhas = qtdFigurinhas;
	}

	public String getImgAlbum() {
		return ImgAlbum;
	}

	public void setImgAlbum(String imgAlbum) {
		ImgAlbum = imgAlbum;
	}

	public Long getId() {
		return id;
	}

	public Album() {
	}
}
