package br.com.figurinhas.dashboard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.figurinhas.dashboard.model.Editora;
import br.com.figurinhas.dashboard.service.EditoraService;

@Controller
public class EditoraController {
	

	@Autowired
	EditoraService servico;
	
	@RequestMapping("/cadastrarEditora")
	public String editoraHome() {
		return "editora/cadastrarEditora";
	}
	

	@RequestMapping(value="salvarEditora", method=RequestMethod.POST)
	public String salvarEditora(HttpServletRequest edit) {
		
		String nome = edit.getParameter("nome");
		
		Editora editora = (Editora) servico.cadastrar(nome);
		
		return editoraHome();
	}

	
	@RequestMapping(value="buscarEditoras", method=RequestMethod.GET)
	public String buscarEditoras(Model model) {
		
		List<Editora> lista = (List<Editora>) servico.buscar();
		
		model.addAttribute("listaEditoras", lista);
		
		return "editora/buscarEditoras";
	}
	
	
	@RequestMapping(value="atualizarEditoraHome/{id}", method=RequestMethod.GET)
	public String atualizarEditoraHome(@PathVariable("id") long id, Model model) {
		
		Editora editora = (Editora) servico.pesquisar(id);
				
		model.addAttribute("editora", editora);
				
		return "editora/atualizarEditora";
	}
	
	@RequestMapping(value="excluirEditora/{id}", method=RequestMethod.GET)
	public String excluirEditora(@PathVariable("id") long id, Model model) {
		
		servico.excluir(id);
		
		return buscarEditoras(model);
	}
	
	@RequestMapping(value="atualizarEditora", method=RequestMethod.POST)
	public String atualizarEditora(HttpServletRequest edit, Model model) {
		
		String nome = edit.getParameter("nome");
		long id = Long.parseLong(edit.getParameter("id"));
		
		Editora editora = new Editora();
		editora.setNome(nome);
		editora.setId((Long)id);
		
		servico.atualizar(editora);
		
		return buscarEditoras(model);
	}
	
}
