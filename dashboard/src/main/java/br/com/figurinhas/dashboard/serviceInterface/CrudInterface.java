package br.com.figurinhas.dashboard.serviceInterface;

public interface CrudInterface {
	
	public Object cadastrar(Object obj);
	public Object buscar();
	public Object atualizar(Object obj);
	public Object pesquisar(Object obj);
	public void excluir(Object id);

}
