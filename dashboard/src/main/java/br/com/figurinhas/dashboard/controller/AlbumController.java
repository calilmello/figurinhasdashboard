package br.com.figurinhas.dashboard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.figurinhas.dashboard.model.Album;
import br.com.figurinhas.dashboard.model.Editora;
import br.com.figurinhas.dashboard.service.AlbumService;
import br.com.figurinhas.dashboard.service.EditoraService;

@Controller
public class AlbumController {

	@Autowired
	AlbumService servico;
	@Autowired
	EditoraService editoraService;

	@RequestMapping("/cadastrarAlbum")
	public String albumHome(Model model) {
		
		List<Editora> lista = (List<Editora>) editoraService.buscar();
		model.addAttribute("listaEditoras", lista);

		return "album/cadastrarAlbum";
	}

	@RequestMapping(value = "salvarAlbum", method = RequestMethod.POST)
	public String salvarAlbum(HttpServletRequest request, Model model) {

		Album album = new Album();
		Editora editora = new Editora();

		editora.setId(Long.parseLong(request.getParameter("editora")));
		
		album.setNome(request.getParameter("nome"));
		album.setAno(Integer.parseInt(request.getParameter("ano")));
		album.setEditora(editora);
		album.setQtdFigurinhas(Integer.parseInt(request.getParameter("qtdFigurinhas")));
		album.setImgAlbum(request.getParameter("imgAlbum"));

		album = (Album) servico.cadastrar(album);

		return albumHome(model);
	}

	@RequestMapping(value = "buscarAlbuns", method = RequestMethod.GET)
	public String buscarAlbuns(Model model) {

		List<Album> lista = (List<Album>) servico.buscar();

		model.addAttribute("listaAlbuns", lista);

		return "album/buscarAlbuns";
	}

	@RequestMapping(value = "atualizarAlbumHome/{id}", method = RequestMethod.GET)
	public String atualizarAlbumHome(@PathVariable("id") long id, Model model) {

		Album album = (Album) servico.pesquisar(id);

		model.addAttribute("album", album);

		return "album/atualizarAlbum";
	}

	@RequestMapping(value = "excluirAlbum/{id}", method = RequestMethod.GET)
	public String excluirAlbum(@PathVariable("id") long id, Model model) {

		servico.excluir(id);
		
		return "redirect:/buscarAlbuns";
	}

	@RequestMapping(value = "atualizarAlbum", method = RequestMethod.POST)
	public String atualizarAlbum(HttpServletRequest request, Model model) {

		
		
		String nome = request.getParameter("nome");
		long id = Long.parseLong(request.getParameter("id"));
		int ano =Integer.parseInt(request.getParameter("ano"));
		int QtdFigurinhas = Integer.parseInt(request.getParameter("qtdFigurinhas"));
		String ImgAlbum = (request.getParameter("imgAlbum"));

		Album album = new Album();
		
		album.setId(id);
		album.setNome(request.getParameter("nome"));
		album.setAno(Integer.parseInt(request.getParameter("ano")));
		album.setQtdFigurinhas(Integer.parseInt(request.getParameter("qtdFigurinhas")));
		album.setImgAlbum(request.getParameter("imgAlbum"));
		
		servico.atualizar(album);

		return buscarAlbuns(model);
	}

}
