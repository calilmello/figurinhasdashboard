package br.com.figurinhas.dashboard.serviceInterface;

import br.com.figurinhas.dashboard.model.Editora;

public interface EditoraServiceInterface {

	public Editora cadastrarEditora(String nome);
	public Editora atualizarEditora(Editora id);
	
}
