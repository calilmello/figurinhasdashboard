package br.com.figurinhas.dashboard.repository;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.figurinhas.dashboard.model.Colecionador;
import br.com.figurinhas.dashboard.repositoryInterface.ColecionadorInterfaceRepository;

@Repository
public class ColecionadorRepository implements ColecionadorInterfaceRepository{

	@Value("${server.api}")
	private String serverApi;
	
	@Override
	public Colecionador cadastrarColecionador(Object obj) {
		
		Colecionador colecionador = (Colecionador) obj;
		
		RestTemplate rest = new RestTemplate();
		HttpEntity<Colecionador> request = new HttpEntity<>(colecionador);
		Colecionador colecionadorResponse = rest.postForObject(serverApi+"/figurinhas/colecionador", request, Colecionador.class);
		
		return colecionadorResponse;
	}


	@Override
	public List<Colecionador> buscarColecionadores() {
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Colecionador[]> response = rest.getForEntity(serverApi+"/figurinhas/colecionador", Colecionador[].class);
		List<Colecionador> listaColecionador = Arrays.asList(response.getBody());
		return listaColecionador;
	}

	@Override
	public Colecionador atualizar(Object obj) {
		
		Colecionador colecionador = (Colecionador) obj;
		RestTemplate rest = new RestTemplate();
		HttpEntity<Colecionador> request = new HttpEntity<>(colecionador);
		rest.put(serverApi+"/figurinhas/colecionador", request);
		
		return colecionador;
	}

	@Override
	public Object pesquisar(Object obj) {
		
		long id = (long) obj;
		
		RestTemplate rest = new RestTemplate();
		ResponseEntity<Colecionador> response = rest.getForEntity(serverApi+"/figurinhas/colecionador/"+id, Colecionador.class);
		Colecionador colecionador = response.getBody();
		
		return colecionador;
	}

	@Override
	public void excluir(Object obj) {
		long id = (long) obj;
		RestTemplate rest = new RestTemplate();
		rest.delete(serverApi+"/figurinhas/colecionador/"+id);
		
	}

}
