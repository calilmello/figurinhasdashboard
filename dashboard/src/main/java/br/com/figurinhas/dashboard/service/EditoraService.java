package br.com.figurinhas.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.dashboard.repository.EditoraRepository;
import br.com.figurinhas.dashboard.serviceInterface.CrudInterface;

@Service
public class EditoraService implements CrudInterface{

	@Autowired
	EditoraRepository repositorio;


	@Override
	public Object cadastrar(Object obj) {
		return repositorio.cadastrarEditora(obj.toString());
	}

	@Override
	public Object buscar() {
		return repositorio.buscarEditoras();
	}

	@Override
	public Object atualizar(Object obj) {
		return repositorio.atualizar(obj);
	}

	@Override
	public Object pesquisar(Object obj) {
		return repositorio.pesquisar(obj);
	}

	@Override
	public void excluir(Object id) {
		repositorio.excluir(id);
	}
	
}
