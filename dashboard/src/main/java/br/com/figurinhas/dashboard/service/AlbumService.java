package br.com.figurinhas.dashboard.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.figurinhas.dashboard.model.Editora;
import br.com.figurinhas.dashboard.repository.AlbumRepository;
import br.com.figurinhas.dashboard.repository.EditoraRepository;
import br.com.figurinhas.dashboard.serviceInterface.CrudInterface;
import br.com.figurinhas.dashboard.serviceInterface.EditoraServiceInterface;

@Service
public class AlbumService implements CrudInterface{

	@Autowired
	AlbumRepository repositorio;


	@Override
	public Object cadastrar(Object obj) {
		return repositorio.cadastrarAlbum(obj);
	}

	@Override
	public Object buscar() {
		return repositorio.buscarAlbum();
	}

	@Override
	public Object atualizar(Object obj) {
		return repositorio.atualizar(obj);
	}

	@Override
	public Object pesquisar(Object obj) {
		return repositorio.pesquisar(obj);
	}

	@Override
	public void excluir(Object id) {
		repositorio.excluir(id);
	}
	
}
