package br.com.figurinhas.dashboard.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.figurinhas.dashboard.model.Album;
import br.com.figurinhas.dashboard.model.Colecao;
import br.com.figurinhas.dashboard.model.Colecionador;
import br.com.figurinhas.dashboard.service.AlbumService;
import br.com.figurinhas.dashboard.service.ColecaoService;
import br.com.figurinhas.dashboard.service.ColecionadorService;

@Controller
public class ColecaoController {
	

	@Autowired
	ColecaoService servico;
	
	@Autowired
	ColecionadorService colecionadorServico;
	
	@Autowired
	AlbumService albumServico;
	
	
	@RequestMapping("/cadastrarColecao")
	public String colecaoHome(Model model) {
		
		List<Colecionador> lista = (List<Colecionador>) colecionadorServico.buscar();
		model.addAttribute("listaColecionador", lista);
		
		List<Album> listaAlbum = (List<Album>) albumServico.buscar();
		model.addAttribute("listaAlbum", listaAlbum);
		
		return "colecao/cadastrarColecao";
	}
	
	@RequestMapping(value="salvarColecao", method=RequestMethod.POST)
	public String salvarColecao(HttpServletRequest request, Model model) {
		
		Colecao colecao = new Colecao();
		Colecionador colecionador = new Colecionador();
		Album album = new Album();
		
		colecionador.setId(Long.parseLong(request.getParameter("colecionador")));
		album.setId(Long.parseLong(request.getParameter("album")));
		
		
		colecao.setAlbum(album);
		colecao.setColecionador(colecionador);
		
		colecao = (Colecao) servico.cadastrar(colecao);
		
		return colecaoHome(model);
	}

	@RequestMapping(value = "buscarColecao", method = RequestMethod.GET)
	public String buscarColecao(Model model) {
		
		List<Colecao> lista = (List<Colecao>) servico.buscar();
		model.addAttribute("listaColecao", lista);
		
		return "colecao/buscarColecao";
	}
	
	@RequestMapping(value="atualizarColecaoHome/{id}", method=RequestMethod.GET)
	public String atualizarColecaoHome(@PathVariable("id") long id, Model model) {
		
		Colecao colecao = (Colecao) servico.pesquisar(id);
				
		model.addAttribute("colecao", colecao);
				
		return "colecao/atualizarColecao";
	}
	
	@RequestMapping(value="excluirColecao/{id}", method=RequestMethod.GET)
	public String excluirColecao(@PathVariable("id") long id, Model model) {
		
		servico.excluir(id);
		
		return buscarColecao(model);
	}
	
	@RequestMapping(value="atualizarColecao", method=RequestMethod.POST)
	public String atualizarColecao(HttpServletRequest edit, Model model) {
		
		String figurinha = edit.getParameter("figurinha");
		long id = Long.parseLong(edit.getParameter("id"));
		
		Colecionador colecionador = new Colecionador();
		Album album = new Album();
		
		Colecao colecao = new Colecao();
		
		colecao.setId((Long)id);
		colecao.setColecionador(colecionador);
		colecao.setAlbum(album);
		colecao.setFigurinha(figurinha);
		
		servico.atualizar(colecao);
		
		return buscarColecao(model);
	}
	
}
