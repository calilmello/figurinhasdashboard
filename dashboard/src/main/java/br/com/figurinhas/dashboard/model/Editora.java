package br.com.figurinhas.dashboard.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize
public class Editora {
	
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("id")
	private Long id;

	@JsonProperty("nome")
	private String nome;
	
	public Editora() {
	}
	
	public Long getId() {
		return id;
	}

	public Editora(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
